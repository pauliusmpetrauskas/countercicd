Deploy a simple web page which would show how many unique visitors has visited the page.

Requirements:

1. The web page should be deployed into a cloud. You can use free or trial tier in AWS or Azure.

2. Code must reside in your private github repository.

3. We must be added as collaborators in order to download the code and test it on our side.

4. The code must be deployable on our side without any modifications to the code

Bonus points:

1. You will get a higher grading if you make the webpage stave its state in redis database

2. You will get a higher grading if you utilize CI/CD pipeline as code
