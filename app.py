import time
from flask import Flask ,request
import redis

app = Flask(__name__)

cache = redis.Redis(host='redis', port=6379)
def get_hit_count():
    
    retries = 5
    while True:
        try:
            return cache.scard("ipaddress")
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc 
            retries -= 1
            time.sleep(0.5)
    
@app.route('/')
def hello():

    cache.sadd("ipaddress", request.remote_addr)
    count = get_hit_count()
    return '<h1>unique visitors {}</h1>'.format(count)